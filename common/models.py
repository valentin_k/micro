import uuid
from datetime import datetime

from gino import Gino
from sqlalchemy.dialects.postgresql import JSONB, UUID


db = Gino()


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(UUID(), unique=True, primary_key=True)
    name = db.Column(db.String(), unique=True, index=True, nullable=False)


class Task(db.Model):
    __tablename__ = 'tasks'

    id = db.Column(
        UUID(),
        primary_key=True,
        default=uuid.uuid4,
        unique=True,
        nullable=False
    )
    author_id = db.Column(UUID(), db.ForeignKey('users.id'), nullable=False)
    assignee_id = db.Column(UUID(), db.ForeignKey('users.id'), nullable=True)
    text = db.Column(db.String())
