# README #

https://github.com/bryanyang0528/ksql-python


https://medium.com/streamthoughts/how-to-build-a-real-time-analytical-platform-using-kafka-ksqldb-and-clickhouse-bfabd65d05e4


https://turkogluc.com/kafka-connect-jdbc-source-connector/

https://turkogluc.com/postgresql-capture-data-change-with-debezium/

https://docs.ksqldb.io/en/latest/tutorials/etl/


mkdir confluent 
curl http://client.hub.confluent.io/confluent-hub-client-latest.tar.gz --output confluent-hub-client-latest.tar.gz
tar -xvf confluent-hub-client-latest.tar.gz --directory confluent

export PATH="$HOME/bin:$PATH"

export PATH="/confluent/bin:$PATH"



https://docs.ksqldb.io/en/latest/tutorials/etl/#get-the-connectors


{
   "type":"record",
   "name":"Value",
   "namespace":"users.public.users",
   "fields":[
      {
         "name":"id",
         "type":{
            "type":"string",
            "connect.version":1,
            "connect.name":"io.debezium.data.Uuid"
         }
      },
      {
         "name":"name",
         "type":"string"
      },
      {
         "name":"hashed_password",
         "type":"string"
      },
      {
         "name":"is_superuser",
         "type":"boolean"
      },
      {
         "name":"__deleted",
         "type":[
            "null",
            "string"
         ],
         "default":null
      }
   ],
   "connect.name":"users.public.users.Value"
}

self made:
'subject' => 'users.public.users-value',
{
   "type":"record",
   "name":"KsqlDataSourceSchema",
   "namespace":"io.confluent.ksql.avro_schemas",
   "fields":[
      {
         "name":"ID",
         "type":[
            "null",
            "string"
         ],
         "default":null
      },
      {
         "name":"NAME",
         "type":[
            "null",
            "string"
         ],
         "default":null
      },
      {
         "name":"HASHED_PASSWOR",
         "type":[
            "null",
            "string"
         ],
         "default":null
      },
      {
         "name":"IS_SUPERUSER",
         "type":[
            "null",
            "boolean"
         ],
         "default":null
      },
      {
         "name":"__DELETED",
         "type":[
            "null",
            "string"
         ],
         "default":null
      }
   ],
   "connect.name":"io.confluent.ksql.avro_schemas.KsqlDataSourceSchema"
}