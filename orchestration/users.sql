CREATE STREAM users_src (
        `id` VARCHAR,
        `name` VARCHAR,
        `hashed_password` VARCHAR,
        `is_superuser` BOOLEAN
    ) WITH (
        kafka_topic = 'users.public.users',
        value_format = 'avro'
    )
;


CREATE TABLE users_by_key AS
    SELECT
        `id`,
        latest_by_offset(`name`) AS `name`,
        latest_by_offset(`is_superuser`) AS `is_superuser`
    FROM users_src
    GROUP BY `id`
    EMIT CHANGES;
