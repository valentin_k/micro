set 'commit.interval.ms'='2000';
-- set 'cache.max.bytes.buffering'='10000000';
set 'auto.offset.reset'='earliest';

-- CREATE STREAM users_src (id VARCHAR, name VARCHAR, hashed_password VARCHAR, is_superuser BOOLEAN)
-- WITH (KAFKA_TOPIC='users',  PARTITIONS=1, REPLICAS=1, VALUE_FORMAT='AVRO');

-- CREATE STREAM users_src_rekey WITH (PARTITIONS=1) AS
-- SELECT * FROM users_src PARTITION BY id;

-- CREATE STREAM USERS_KANBAN WITH (PARTITIONS=1) AS
-- SELECT id as `id`, name as `name`, is_superuser as `is_superuser` FROM users_src_rekey;

-- CREATE SOURCE CONNECTOR postgres_users_source WITH (
--     'connector.class' = 'io.debezium.connector.postgresql.PostgresConnector',
--     'database.hostname' = 'postgres',
--     'database.port' = '5432',
--     'database.user' = 'postgres',
--     'database.password' = 'password',
--     'database.dbname' = 'auth',

--     'database.server.name' = 'users',    
    
--     'table.whitelist' = 'public.users',
--     'transforms' = 'unwrap',
--     'transforms.unwrap.type' = 'io.debezium.transforms.ExtractNewRecordState',
--     'transforms.unwrap.drop.tombstones' = 'false',
--     'transforms.unwrap.delete.handling.mode' = 'rewrite'

-- );


-- CREATE STREAM users_src 
--     WITH (
--         kafka_topic = 'users.public.users',
--         value_format = 'avro',
--         partitions=1,
--         replicas=1
--     )
-- ;


-- CREATE STREAM users_src (
--         `id` VARCHAR,
--         `name` VARCHAR,
--         `hashed_password` VARCHAR,
--         `is_superuser` BOOLEAN
--     ) WITH (
--         kafka_topic = 'users.public.users',
--         value_format = 'avro'
--     )
-- ;


-- CREATE TABLE users_by_key AS
--     SELECT
--         `id`,
--         latest_by_offset(`name`) AS `name`,
--         latest_by_offset(`is_superuser`) AS `is_superuser`
--     FROM users_src
--     GROUP BY id
--     EMIT CHANGES;



-- CREATE SOURCE CONNECTOR postgres_users_source WITH (
--     'connector.class' = 'io.debezium.connector.postgresql.PostgresConnector',
--     'tasks.max' = '1',
--     'database.hostname' = 'postgres',
--     'database.port' = '5432',
--     'database.user' = 'postgres',
--     'database.password' = 'password',
--     'database.dbname' = 'auth',

--     'database.allowPublicKeyRetrieval' = 'true',
--     -- 'database.server.id' = '184054',

--     'database.server.name' = 'debezium1',
--     'database.whitelist' = 'users',
--     'database.history.kafka.bootstrap.servers' = 'kafka:9092',
--     'database.history.kafka.topic' = 'users',
--     'table.include.list' = 'public.users',
--     'include.schema.changes' = 'false',
--     'key.converter' = 'org.apache.kafka.connect.storage.StringConverter',
--     'value.converter' = 'io.confluent.connect.avro.AvroConverter',
--     'key.converter.schemas.enable' = 'false',
--     'value.converter.schemas.enable' = 'true',
--     'value.converter.schema.registry.url' = 'http://schema-registry:8081',
--     'transforms' = 'unwrap',
--     'transforms.unwrap.type' = 'io.debezium.transforms.ExtractNewRecordState'
-- );


-- {"name": "postgres_users_source",
--   "config": {
--     "connector.class":"io.debezium.connector.postgresql.PostgresConnector",
--     "tasks.max":"1",
--     "database.hostname": "postgres",
--     "database.port": "5432",
--     "database.user": "postgres",
--     "database.password": "password",
--     "database.dbname" : "auth",
--     "database.server.name": "debezium1",
--     "database.whitelist": "users",
--     "database.history.kafka.bootstrap.servers": "kafka:9092",
--     -- !
--     "database.history.kafka.topic": "schema-changes.users",
--     -- 
--     "key.converter": "org.apache.kafka.connect.storage.StringConverter",
--     "value.converter": "io.confluent.connect.avro.AvroConverter",
--     "key.converter.schemas.enable": "false",
--     "value.converter.schemas.enable": "true",
--     "value.converter.schema.registry.url": "http://schema-registry:8081",
--     "transforms": "unwrap",
--     "transforms.unwrap.type": "io.debezium.transforms.ExtractNewRecordState"
--   }
-- }


CREATE SOURCE CONNECTOR postgres_users_sink WITH (
    'connector.class' = 'io.confluent.connect.jdbc.JdbcSinkConnector',

    'tasks.max' = '1',
    'topics' = 'USERS_BY_KEY',
    'key.converter' = 'org.apache.kafka.connect.storage.StringConverter',
    'value.converter' = 'io.confluent.connect.avro.AvroConverter',
    'value.converter.schema.registry.url' = 'http://schema-registry:8081',
    'connection.url' = 'jdbc:postgresql://postgres:5432/kanban?user=postgres&password=password&stringtype=unspecified',
    'key.converter.schemas.enable' = 'false',
    'value.converter.schemas.enable' = 'true',
    'auto.create' = 'true',
    'auto.evolve' = 'true',
    'insert.mode' = 'upsert',
    'pk.fields' = 'id',
    'pk.mode' = 'record_key',

    'fields.whitelist'= 'id,name,is_superuser',
    'table.name.format'= 'users'

);