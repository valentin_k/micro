#!/bin/bash
echo "Preparing Kafka"

while ! nc -z ksqldb-server 8088;
do
    sleep 1;
done;
echo KSQL operational!;


sleep 20;

echo Running ksql transaction;

ksql http://ksqldb-server:8088 <<< "RUN SCRIPT /code/initial.sql;";
sleep 3;
ksql http://ksqldb-server:8088 <<< "RUN SCRIPT /code/users_source.sql;";
sleep 3;
ksql http://ksqldb-server:8088 <<< "RUN SCRIPT /code/users.sql;";
sleep 2;
ksql http://ksqldb-server:8088 <<< "RUN SCRIPT /code/users_sink.sql;";

# curl -X POST -H "Accept:application/json" -H "Content-Type: application/json" --data @./code/postgres_users_source.json http://connect:8083/connectors;
# curl -X POST -H "Accept:application/json" -H "Content-Type: application/json" --data @./code/postgres_users_sink.json http://connect:8083/connectors;


echo "Kafka prepared"

while true;
    do sleep 10000;
done