CREATE SOURCE CONNECTOR postgres_users_sink WITH (
    'connector.class' = 'io.confluent.connect.jdbc.JdbcSinkConnector',

    'tasks.max' = '1',
    'topics' = 'USERS_BY_KEY',
    'key.converter' = 'org.apache.kafka.connect.storage.StringConverter',
    'value.converter' = 'io.confluent.connect.avro.AvroConverter',
    'value.converter.schema.registry.url' = 'http://schema-registry:8081',
    'connection.url' = 'jdbc:postgresql://postgres:5432/kanban?user=postgres&password=password&stringtype=unspecified',
    'key.converter.schemas.enable' = 'false',
    'value.converter.schemas.enable' = 'true',
    'auto.create' = 'true',
    'auto.evolve' = 'true',
    'insert.mode' = 'upsert',
    'pk.fields' = 'id',
    'pk.mode' = 'record_key',

    'fields.whitelist'= 'id,name,is_superuser',
    'table.name.format'= 'users'

);
