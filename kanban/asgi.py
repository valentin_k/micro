import asyncio
from typing import NewType, Optional, List
import uuid

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.responses import Response
from fastapi.security import OAuth2PasswordBearer
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from fastapi_utils.api_model import APIModel

from models import db, Task, User as UserModel
from pydantic import BaseModel
import settings


app = FastAPI()


oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl=f'{settings.AUTH_URI}/token'
)


class User(BaseModel):
    # TODO
    # https://pydantic-docs.helpmanual.io/datamodel_code_generator/
    id: uuid.UUID
    name: str
    is_superuser: bool


async def fake_decode_token(token):
    user = await UserModel.query.where(
        UserModel.id == token
    ).gino.one_or_none()

    return User(**user.to_dict())


async def get_current_user(token: str = Depends(oauth2_scheme)):
    user = await fake_decode_token(token)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Invalid authentication credentials',
            headers={'WWW-Authenticate': 'Bearer'},
        )
    return user


async def get_current_active_user(current_user: User = Depends(get_current_user)):
    # TODO
    # https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt/
    
    # if current_user.disabled:
    #     raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


@app.on_event('startup')
async def startup_event():
    await db.set_bind(
        'postgresql://{role}:{password}@{host}:{port}/{database}'.format(
            **settings.POSTGRES_CREDS
        )
    )
    await db.gino.create_all()

    # заводим кафку с толчка
    import uuid
    try:
        user = await UserModel.create(
            id=uuid.UUID('06335e84-2872-4914-8c5d-3ed07d2a2f16'),
            name='username',
            is_superuser=True
        )

        await Task.create(
            text = 'task.text',
            author_id = user.id
        )
    except:
        pass


@app.on_event('shutdown')
async def shutdown_event():
    await db.pop_bind().close()


@app.get('/users/me')
async def read_users_me(current_user: User = Depends(get_current_active_user)):
    return current_user


# TODO: move to separate file
router = InferringRouter()

TaskID = NewType('TaskID', uuid.UUID)
UserID = NewType('UserID', uuid.UUID)


class TaskRetrieve(APIModel):
    id: TaskID
    author_id: UserID
    assignee_id: Optional[UserID]
    text: str
    is_done: bool


class TaskCreate(APIModel):
    text: str


async def is_superuser(current_user: User = Depends(get_current_active_user)):
    if not current_user.is_superuser:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)


@cbv(router)
class ClassCBV:
    current_user: User = Depends(get_current_active_user)

    @router.get('/tasks/my')
    async def retrieve_assigned_tasks(self) -> List[TaskRetrieve]:
        tasks = await Task.query.where(
            Task.assignee_id == self.current_user.id
        ).gino.all()

        return tasks
    
    @router.post('/tasks/reassign', dependencies=[Depends(is_superuser)])
    async def reassign_task(self):
        await db.status(db.text('''
            WITH subquery AS (
                with ranked_users as (
                    select *, row_number() OVER () r
                    from users
                ),
                shuffled_tasks as (
                    with counter as (
                        SELECT COUNT(*) users_count
                        FROM users
                    )
                    select
                        id,
                        is_done,
                        floor(random() * users_count + 1)::int assigned
                    from
                        tasks
                        NATURAL JOIN counter
                    WHERE
                        tasks.is_done = false
                )
                select shuffled_tasks.id as task_id, ranked_users.id as user_id
                from ranked_users join shuffled_tasks
                on ranked_users.r = shuffled_tasks.assigned
            )
            UPDATE tasks
            SET assignee_id = subquery.user_id
            FROM subquery
            WHERE
                tasks.id = subquery.task_id;
        '''))

        return Response(status_code=status.HTTP_204_NO_CONTENT)

    @router.get('/tasks/{task_id}')
    async def retrieve_task(self, task_id: TaskID) -> TaskRetrieve:
        task = await Task.query.where(
            Task.id == task_id
        ).gino.one_or_none()
        if task is None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

        return TaskRetrieve.from_orm(task)
    
    @router.get('/tasks')
    async def retrieve_tasks(self) -> List[TaskRetrieve]:
        tasks = await Task.query.gino.all()

        return tasks

    @router.post('/tasks')
    async def create_task(self, task: TaskCreate) -> TaskRetrieve:
        created_task = await Task.create(
            text = task.text,
            author_id = self.current_user.id
        )

        return created_task
    
    @router.patch('/tasks/{task_id}/mark_as_done')
    async def mark_as_done(self, task_id: TaskID):
        result = await Task.update.values(
            is_done=True
        ).where(db.and_(
            Task.id == task_id,
            Task.is_done == False,
            Task.assignee_id == self.current_user.id
        )).gino.status()
        if result[0] != 'UPDATE 1':
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)
        
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    

app.include_router(router)
