import os
from secrets import token_urlsafe


POSTGRES_CREDS = {
    'role': os.environ.get('POSTGRES_ROLE', ''),
    'password':  os.environ.get('POSTGRES_PASSWORD', ''),
    'port': 5432,
    'host': 'postgres',
    'database': os.environ.get('POSTGRES_DATABASE', 'kanban'),
}
AMQP_CREDS = {
    'login': 'guest',
    'password': 'guest',
    'host': 'rabbit',
}
AUTH_URI = 'http://localhost:8001'
SECRET = token_urlsafe(32)
