docker-compose up -d



curl -i -X POST -H "Accept:application/json" -H "Content-Type:application/json" localhost:8083/connectors/ -d @postgresql-connect.json
# curl -H "Accept:application/json" localhost:8083/connectors/

# docker-compose exec kafka bash
# /usr/bin/kafka-topics --list --zookeeper zookeeper:2181

docker run --network postgres-kafka-demo_default \
           --interactive --tty --rm \
           confluentinc/cp-ksql-cli:latest \
           http://ksqldb-server:8088


ksql http://ksqldb-server:8088
set 'commit.interval.ms'='2000';
set 'cache.max.bytes.buffering'='10000000';
set 'auto.offset.reset'='earliest';




SHOW TOPICS;

# CREATE STREAM users_src (id VARCHAR, name VARCHAR, hashed_password VARCHAR)\
# WITH (KAFKA_TOPIC='debezium1.public.users',  PARTITIONS=1, REPLICAS=1, VALUE_FORMAT='JSON');
CREATE STREAM users_src (id VARCHAR, name VARCHAR, hashed_password VARCHAR)\
WITH (KAFKA_TOPIC='debezium1.public.users',  PARTITIONS=1, REPLICAS=1, VALUE_FORMAT='AVRO');

CREATE STREAM users_src_rekey WITH (PARTITIONS=1) AS \
SELECT * FROM users_src PARTITION BY id;



# CREATE STREAM needs_quotes \
# (`id` VARCHAR, `name` VARCHAR, `hashed_password` INT) \
# WITH (KAFKA_TOPIC='debezium1.public.users',  PARTITIONS=1, REPLICAS=1, VALUE_FORMAT='AVRO');


SHOW STREAMS;




# CREATE TABLE users (id VARCHAR, name VARCHAR, hashed_password VARCHAR)\
# WITH (KAFKA_TOPIC='USERS_SRC_REKEY', VALUE_FORMAT='JSON', KEY='id');
# CREATE TABLE users (id VARCHAR, name VARCHAR, hashed_password VARCHAR)\
# WITH (KAFKA_TOPIC='USERS_SRC_REKEY', VALUE_FORMAT='AVRO', KEY='id');

# CREATE TABLE users2 (`id` VARCHAR, `name` VARCHAR, `hashed_password` VARCHAR)\
# WITH (KAFKA_TOPIC='USERS_SRC_REKEY', VALUE_FORMAT='AVRO', KEY='`id`');

CREATE STREAM users_kanban WITH (PARTITIONS=1) AS \
SELECT id as `id`, name as `name` FROM users_src_rekey;

# CREATE TABLE users2 (`id` VARCHAR, `name` VARCHAR, `hashed_password` VARCHAR)\
# WITH (KAFKA_TOPIC='USERS_SRC_REKEY', VALUE_FORMAT='AVRO', KEY='`id`');

select * from users EMIT CHANGES;


# CREATE TABLE users_kanban AS \
# SELECT id, name \
# FROM users \
# WITH (KAFKA_TOPIC='users', VALUE_FORMAT='delimited', KEY='id');


CREATE TABLE users_kanbanx (`id` VARCHAR, `name` VARCHAR)\
WITH (KAFKA_TOPIC='USERS_KANBAN', VALUE_FORMAT='delimited');
# тут нету ключа 


curl -X POST -H "Accept:application/json" -H "Content-Type: application/json" \
      --data @postgres_users_source.json http://localhost:8083/connectors

curl -X POST -H "Accept:application/json" -H "Content-Type: application/json" \
      --data @postgres_users_sink.json http://localhost:8083/connectors


# --------------

CREATE STREAM admission_src_rekey WITH (PARTITIONS=1) AS \
SELECT * FROM admission_src PARTITION BY student_id;

CREATE TABLE admission (student_id VARCHAR, gre INTEGER, toefl INTEGER, cpga DOUBLE, admit_chance DOUBLE)\
WITH (KAFKA_TOPIC='ADMISSION_SRC_REKEY', VALUE_FORMAT='AVRO', KEY='student_id');

# --------------

