import uuid
from datetime import datetime

from gino import Gino
from sqlalchemy.dialects.postgresql import UUID


db = Gino()


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(UUID(), unique=True, primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String(), unique=True, index=True, nullable=False)
    hashed_password = db.Column(db.String(), nullable=False)
    is_superuser = db.Column(db.Boolean(), nullable=False)
