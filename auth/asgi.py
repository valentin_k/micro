from typing import Optional

from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from fastapi.middleware.cors import CORSMiddleware
from fastapi import Depends, FastAPI, HTTPException, Form
from fastapi.security import OAuth2PasswordRequestForm
from models import db, User
from asyncpg.exceptions import UniqueViolationError
import settings


app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event('startup')
async def startup_event():
    await db.set_bind(
        'postgresql://{role}:{password}@{host}:{port}/{database}'.format(
            **settings.POSTGRES_CREDS
        )
    )
    await db.gino.create_all()

    # заводим кафку с толчка
    import uuid
    try:
        await User.create(
            id=uuid.UUID('06335e84-2872-4914-8c5d-3ed07d2a2f16'),
            name='username',
            hashed_password='password_hasher.hash(password)',
            is_superuser=True
        )
    except:
        pass


@app.on_event('shutdown')
async def shutdown_event():
    await db.pop_bind().close()


@app.post('/token')
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    password_hasher = PasswordHasher()

    user = await User.query.where(
        User.name == form_data.username
    ).gino.one_or_none()
    if user is None:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    
    try:
        password_hasher.verify(user.hashed_password, form_data.password)
    except VerifyMismatchError:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    return {"access_token": user.id, "token_type": "bearer"}


@app.post("/register")
async def register(
    username: str = Form(...),
    password: str = Form(...),
    is_superuser: bool = Form(...)
):
    password_hasher = PasswordHasher()
    
    try:
        user = await User.create(
            name=username,
            hashed_password=password_hasher.hash(password),
            is_superuser=is_superuser
        )
    except UniqueViolationError:
        return {'ok': 'not ok'}

    return {'ok': 'ok'}
